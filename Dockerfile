FROM pytorch/pytorch:1.4-cuda10.1-cudnn7-devel
WORKDIR /NSVF
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
#COPY . .
#RUN pip install --editable ./
#RUN python setup.py build_ext --inplace